/*
Register form submit
*/
$('body').on('click', '#submitForm', function(){
    var registerForm = $("#Register");
    var formData = registerForm.serialize();
    $( '#name-error' ).html( "" );
    $( '#surname-error' ).html( "" );
    $( '#patronymic-error' ).html( "" );
    $( '#addres-error' ).html( "" );
    $( '#email-error' ).html( "" );
    $( '#login-error' ).html( "" );
    $.ajax({
        url:'/register',
        type:'POST',
        data:formData,
        success:function(data) {
            console.log(data);
            if(data.errors) {
                $( '#login-error' ).html( '' );
                if(data.errors.name){
                    $( '#name-error' ).html( data.errors.name[0] );
                }
                if(data.errors.surname){
                    $( '#surname-error' ).html( data.errors.surname[0] );
                }
                if(data.errors.patronymic){
                    $( '#patronymic-error' ).html( data.errors.patronymic[0] );
                }
                if(data.errors.addres){
                    $( '#addres-error' ).html( data.errors.addres[0] );
                }
                if(data.errors.email){
                    $( '#email-error' ).html( data.errors.email[0] );
                }
                if(data.errors.login){
                    $( '#login-error' ).html( data.errors.login[0] );
                }
                
            }
            if(data.success) {
                $('#register-form').remove();
                $('#confirm-register-code').removeClass('hide');
                $('#success-msg-register').removeClass('hide');
                setInterval(function(){
                    $('#success-msg-register').addClass('hide');
                }, 5000);                
            }
        },
    });
});

/*
Confirm Register form submit
*/

function sendCodeRegisterFunction()
{
    var registerForm = $("#ConfirmRegister");
    var formData = registerForm.serialize();
    $( '#code-error' ).html( '' );
    $.ajax({
        url:'/verificate',
        type:'POST',
        data:formData,
        success:function(data) {
            console.log(data);
            if(data.errors) {
                if(data.errors.code){
                    $( '#code-error' ).html( data.errors.code[0] );
                }
            }
            
            if(data.success == 'Y') {
                $('#success-msg').removeClass('hide');
                setInterval(function(){ 
                    $('#RegisterModal').modal('hide');
                    $('#success-msg').addClass('hide');
                    location.reload();
                }, 3000);
            }
        },
    });
    return false;
}

$('body').on('submit', '#ConfirmRegister', function(){
    sendCodeRegisterFunction();
    return false;
});

$('body').on('click', '#confirmForm', function(){
    sendCodeRegisterFunction();
    return false;
});

/*
Login form submit
*/

function sendPhoneFunction()
{
    var registerForm = $("#Login");
    var formData = registerForm.serialize();
    $( '#login-login-error' ).html( "" );
    $.ajax({
        url:'/login',
        type:'POST',
        data:formData,
        success:function(data) {
            console.log(data);
            if(data.errors) {
                if(data.errors.login){
                    $( '#login-login-error' ).html( data.errors.login[0] );
                }                
            }
            if(data.success) {
                $('#login-form').remove();
                $('#confirm-login-code').removeClass('hide');
            }
        },
    });
    return false;
}

$('body').on('submit', '#Login', function(){
    sendPhoneFunction();
    return false;
});

$('body').on('click', '#submitLoginForm', function(){
    sendPhoneFunction();
    return false;
});

/*
Confirm Login form submit
*/

function sendCodeLoginFunction()
{
    var registerForm = $("#ConfirmLogin");
    var formData = registerForm.serialize();
    
    $.ajax({
        url:'/verificate',
        type:'POST',
        data:formData,
        success:function(data) {
            console.log(data);
            if(data.errors) {
                $( '#login-code-error' ).html( '' );
                if(data.errors.code){
                    $( '#login-code-error' ).html( data.errors.code[0] );
                }
                
            }
            if(data.success == 'Y') {
                $('#login-success-msg').removeClass('hide');
                $('#confirm-login-code').addClass('hide');
                setInterval(function(){ 
                    //$('#ConfirmLogin').modal('hide');
                    //$('#success-msg').addClass('hide');
                    location.reload();
                }, 3000);
            }
        },
    });
    return false;
}

$('body').on('submit', '#ConfirmLogin', function(){
    sendCodeLoginFunction();
    return false;
});

$('body').on('click', '#confirmLoginForm', function(){
    sendCodeLoginFunction();
    return false;
});

/*
Confirm Login form submit
*/

function sendCheckFunction()
{
    var registerForm = $("#AddCheck");
    var formData = new FormData()
    var params = registerForm.serializeArray();
    var files    = registerForm.find('[name="photo"]')[0].files;

    $.each(files, function(i, file) {
        // Prefix the name of uploaded files with "uploadedFiles-"
        // Of course, you can change it to any string
        formData.append('photo', file);
    });
    $.each(params, function(i, val) {
        formData.append(val.name, val.value);
    });
    //formData.append('photo', registerForm.prop('files')[0]);
    $( '#unp-error' ).html( '' );
    $( '#buy_time-error' ).html( '' );
    $( '#serial-error' ).html( '' );
    $( '#photo-error' ).html( '' );
    $.ajax({
        url:'/check',
        type:'POST',
        cache: false,
        contentType: false,
        processData: false,
        data:formData,
        success:function(data) {
            console.log(data);
            if(data.errors) {
                
                if(data.errors.unp){
                    $( '#unp-error' ).html( data.errors.unp[0] );
                }
                if(data.errors.buy_time){
                    $( '#buy_time-error' ).html( data.errors.buy_time[0] );
                }
                if(data.errors.serial){
                    $( '#serial-error' ).html( data.errors.serial[0] );
                }
                if(data.errors.photo){
                    $( '#photo-error' ).html( data.errors.photo[0] );
                }
                
            }
            if(data.success == 'Y') {
                alert('Чек добавлен');
                setInterval(function(){ 
                    //$('#ConfirmLogin').modal('hide');
                    //$('#success-msg').addClass('hide');
                    location.reload();
                }, 3000);
            }
        },
    });
    return false;
}

$('body').on('submit', '#AddCheck', function(){
    sendCheckFunction();
    return false;
});

$('body').on('click', '#submitAddCheckForm', function(){
    sendCheckFunction();
    return false;
});

$(function () {
    $('#datetimepicker1, .input-group.date').datetimepicker({
        locale: 'ru'
    });
});