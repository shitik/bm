<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Check;
use Excel;

class ExportChecks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $requestData;

    public function __construct($requestData)
    {
        //
        $this->requestData = $requestData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $requestData = $this->requestData;
        if (!isset($requestData['deleted']))
        {
            $requestData['deleted'] = false;
        }

        if (!isset($requestData['deleted']))
        {
            $requestData['deleted'] = false;
        }

        if (!isset($requestData['created_at_from']))
        {
            $requestData['created_at_from'] = false;
        }

        if (!isset($requestData['created_at_to']))
        {
            $requestData['created_at_to'] = false;
        }

        if ($requestData['deleted'])
        {
            $data = Check::onlyTrashed()->with('user')->createFrom($requestData['created_at_from'])->createUntil($requestData['created_at_to'])->get();
        }else{
            $data = Check::where('active', true)->with('user')->createFrom($requestData['created_at_from'])->createUntil($requestData['created_at_to'])->get();
        }
        
        //$attributes = $data->attributesToArray();
        //$newadta = array_merge($attributes, $data->relationsToArray());
        //->toArray();
        
        $exportData = [];
        $exportData[] = ['id', 'УНП', 'Время покупки', 'Номер', 'Загружено', 'Имя', 'Фамилия', 'Отчество', 'Телефон', 'Адрес'];
        foreach($data as $row)
        {
            //echo "<pre>"; print_r(array_merge($row->attributesToArray(), $row->relationsToArray())); echo "</pre>";
            //$row->attributesToArray();
            $newRow = array_only($row->toArray(), ['id', 'unp', 'serial', 'buy_time', 'created_at']);
            
            //echo "<pre>"; print_r($newRow); echo "</pre>";
            //return "1";
            //unset($newRow['user']);
            $newRow['name'] = $row->user->name;
            $newRow['surname'] = $row->user->surname;
            $newRow['patronymic'] = $row->user->patronymic;
            $newRow['phone'] = $row->user->login;
            $newRow['addres'] = $row->user->addres;
            
            $exportData[] = $newRow;
        }
        //echo "<pre>"; print_r($exportData); echo "</pre>";
        //return "1";
        //(new Collection($exportData))->downloadExcel('export', 'Xls');
        Excel::create('checks-'.$requestData['name'], function($excel) use($exportData) {

            $excel->sheet('Sheetname', function($sheet) use($exportData) {

                $sheet->fromArray($exportData);

            });

        })->store('xls', storage_path('app/excel/exports'));
        return true;
    }
}
