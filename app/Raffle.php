<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
    //
    protected $fillable = [
        'name', 'active', 'sms_date', 'start', 'end'
    ];

    public function winners()
    {
        return $this->hasMany('App\Winner'); 
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public static function getActiveId()
    {
        return Raffle::has('winners')->active()->orderBy('id', 'desc')->first()->id;
    }
}
