<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'login', 'name', 'surname', 'patronymic', 'addres', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $rocketUsername = '690655159';
    protected $rocketPassword = 'eBg5he67';
    protected $digits = 6;

    public function sendRocketSMS($message) {
        //return true;
        $phone = str_replace('+', '', $this->login);
        $curl = curl_init();
    
        curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');   
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "username=".$this->rocketUsername."&password=".$this->rocketPassword . "&priority=true&phone=" . $phone . "&text=" . $message);
        
        $result = @json_decode(curl_exec($curl), true);
        
        if ($result && isset($result['id'])) {
            return true;//"Message has been sent. MessageID=" . $result['id'];
        } elseif ($result && isset($result['error'])) {
            return false;//"Error occurred while sending message. ErrorID=" . $result['error'];
        } else {
            return false;//"Service error";
        }
    }
    
    public function verification()
    {
        return $this->hasOne('App\Verification'); 
    }

    public function check()
    {
        return $this->hasMany('App\Check'); 
    }

    public function verificate($code)
    {
        if (!$this->verification)
        {
            $this->sendActivationCode();
            return false;
        }
        if ($this->verification->code == $code)
        {
            $this->verification->delete();
            return true;
        }
        return false;
        return ($this->verification->code === $code) ? true : false;
    }

    public function generateCode()
    {
        $digits = $this->digits;
        $code = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        return $code;
    }

    public function sendActivationCode()
    {
        $code = $this->generateCode();
        if ($this->verification)
            $this->verification->update(['code' => $code]);
        else
            $this->verification()->create(['code' => $code]);
        $this->sendRocketSMS('Пароль для входа в личный кабинет promo.bambolina.by: '.$code);
        return true;
    }
}
