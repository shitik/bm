<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Image;
use Carbon\Carbon;

class Check extends Model
{
    //
    use SoftDeletes;


    protected $fillable = [
        'unp', 'buy_time', 'serial', 'photo', 'reason'
    ];

    protected $dates = ['deleted_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function winners()
    {
        return $this->hasMany('App\Winner'); 
    }

    public function scopeCreateFrom($query, $date)
    {
        if ($date !== NULL && $date != '')
            return $query->where('created_at', '>', Carbon::parse($date));
        return $query;
    }

    public function scopeCreateUntil($query, $date)
    {
        if ($date !== NULL && $date != '')
            return $query->where('created_at', '<=', Carbon::parse($date));
        return $query;
    }

    public function getPhotoAttribute($value)
    {
        if (substr($value, -4) == '.pdf')
            return Storage::url($value);
    	return Image::make(Storage::get($value))->encode('data-url');
    }

    public function getPhotoPathAttribute()
    {
        return Storage::url($this->getOriginal()['photo']);   
    }

    public function getImageAttribute()
    {
        if (substr($this->getOriginal()['photo'], -4) == '.pdf')
            return false;
        return true;
    }

    public function approve($count=1)
    {
        $this->active = true;
        $this->save();
        $raffle = Raffle::where('start', '<=', Carbon::parse($this->created_at))->where('end', '>=', Carbon::parse($this->created_at))->first();
        if (!$raffle)
        {
            return false;
        }
        $this->user->sendRocketSMS($this->smsText($this->id, $raffle->sms_date));
        if ($count > 1)
        {
            for ($i=1; $i<$count; $i++)
            {
                $newCheck = $this->replicate();
                $newCheck->created_at = $this->created_at;
                $newCheck->save();
                $this->user->sendRocketSMS($this->smsText($newCheck->id, $raffle->sms_date));
            }
        }
        //$this->user->sendRocketSMS('Ваш чек номер: '.$this->id.'. Одобрен.');
        return true;
    }

    public function decline($reason)
    {
        $this->reason = $reason;
        $this->save();
        $this->user->sendRocketSMS('Ваш чек №'.$this->id.' отклонен. Причина: '.$reason);
        $this->delete();
        return true;
    }

    private function smsText($id, $name)
    {
        return 'Ваш код участника: '.$id.'. Розыгрыш призов '.$name.'. Играй с BAMBOLINA!';
    }

    public function sendApprovedSms($count)
    {
        $raffle = Raffle::where('start', '<=', Carbon::parse($this->created_at))->where('end', '>=', Carbon::parse($this->created_at))->first();
        if (!$raffle)
        {
            return false;
        }
        if ($count > 0)
        {
            for ($i=1; $i<=$count; $i++)
            {
                $newCheck = $this->replicate();
                $newCheck->created_at = $this->created_at;
                $newCheck->save();
                $this->user->sendRocketSMS($this->smsText($newCheck->id, $raffle->sms_date));
            }
        }

    }

}
