<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    //
    protected $fillable = [
        'check_id', 'prize_id'
    ];

    public function raffle()
    {
    	return $this->belongsTo('App\Raffle');
    }

    public function prize()
    {
    	return $this->belongsTo('App\Prize');
    }

    public function check()
    {
    	return $this->belongsTo('App\Check');
    }
}
