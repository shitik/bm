<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/^((8|\+375)[\- ]?)?(\(?\d{2}\)?[\- ]?)?[\d\- ]{9,12}$/", $value);
            return $value == 'foo';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
