<?php

namespace App\Http\Controllers;

use App;
use App\User;
use Illuminate\Http\Request;
use Response;
use Auth;

class PhoneVerificationController extends Controller {

	private $User;

	function __construct(User $User) {
		$this->User = $User;
	}

	public static function formatPhoneNumber(&$phone)
    {
        $phone = preg_replace("/[^0-9]/", '', $phone);
        if (substr($phone, 0, 2) == 80)
            $phone = '+375'.substr($phone, 2);
        elseif (substr($phone, 0, 1) == 8)
            $phone = '+375'.substr($phone, 1);
        else
            $phone = '+'.$phone;
    }

	/**
	 * Select verification type.
	 * @param Request $request
	 * @return json
	 */
	public function index(Request $request) {
		
		$user = $this->User->find($request->session()->get('user_id'));
		if (!$user)
		{
			if ($request->activate)
			{
				return view('personal.verificate')->withErrors(['code' => ['Что то пошло не так']]);
			}
			return Response::json(['errors' => ['code' => ['Что то пошло не так']]]);
		}
		if ($user->verificate($request->code))
		{
			$user->active = true;
			$user->save();
			Auth::login($user);
			if ($request->activate)
			{
				return redirect()->route('user.add.check');
			}
			return Response::json(['success' => 'Y']);
		}
		else
		{
			if ($request->activate)
			{
				return view('personal.verificate')->withErrors(['code' => ['Неверный код подтверждения']]);
			}
			return Response::json(['errors' => ['code' => ['Неверный код подтверждения']]]);
		}
	}
}
