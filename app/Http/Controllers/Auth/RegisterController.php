<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PhoneVerificationController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Response;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/personal/admin/check';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */


    protected function create(array $data)
    {
        $data['addres'] = $data['index'].', '.$data['city'].', '.$data['street'].', д. '.$data['building'].', к. '.$data['corpus'].', кв. '.$data['appart'];
        PhoneVerificationController::formatPhoneNumber($data['login']);
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'patronymic' => $data['patronymic'],
            'addres' => $data['addres'],
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => bcrypt(str_random(10)),
        ]);
        if ($user)
            $user->sendActivationCode();

        return $user;

    }

    public function register(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'patronymic' => 'required|max:255',
            'index' => 'required|max:255',
            'city' => 'required|max:255',
            'street' => 'required|max:255',
            'building' => 'required|max:255',
            'login' => 'required|phone|max:20|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'persData' => 'required',
            'rules' => 'required',
        ]);


        if ($validator->passes()) {
            $user = $this->create($input);
                
            // Store your user in database 

            if ($user)
            {
                $request->session()->put('user_id', $user->id);
                return view('personal.verificate');
                return Response::json(['success' => '1']);
            }
            else
                return Response::json(['success' => '0']);

        }
        $validator->validate();
        return Response::json(['errors' => $validator->errors()]);
    }
}
