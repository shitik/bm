<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PhoneVerificationController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/check';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        if ($user && $user->login === 'admin')
        {
            $this->validateLogin($request);

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
        else
        {
            $input = $request->all();
            PhoneVerificationController::formatPhoneNumber($input['login']);
            $validator = Validator::make($input, [
                'login' => 'required|phone|max:20',
            ]);
            if ($validator->passes()) {
                //$this->formatPhoneNumber($input['login']);
                if ($user = User::where('login', $input['login'])->first())
                {
                    $user->sendActivationCode();
                    $request->session()->put('user_id', $user->id);
                    return Response::json(['success' => '1']);
                }
                else
                    return Response::json(['errors' => ['login' => ['Телефон не найден, <a href="/register">зарегистрируйтесь</a>']]]);
            }
        
            return Response::json(['errors' => $validator->errors()]);
        }
    }
}
