<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use Auth;
use Storage;
use File;
use Artisan;
use Carbon\Carbon;
use App\Check;
use App\Prize;
use App\Raffle;
use App\Winner;
use App\Jobs\ExportChecks;
//use Illuminate\Support\Collection;
use Excel;

class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Check $Check, Prize $Prize, Raffle $Raffle, Winner $Winner)
    {
        $this->Check = $Check;
        $this->Prize = $Prize;
        $this->Raffle = $Raffle;
        $this->Winner = $Winner;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function viewCheckFile(Check $check, Request $request)
    {
        $filePath = $check->getOriginal()['photo'];
        if( ! Storage::exists($filePath) ) {
            abort(404);
        }

        $pdfContent = Storage::get($filePath);

        // for pdf, it will be 'application/pdf'
        $type       = Storage::mimeType($filePath);
        $fileName   = "12121.pdf";//Storage::name($filePath);

        return Response::make($pdfContent, 200, [
          'Content-Type'        => $type,
          'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);
        //return response()->download($check->photo_path);
        return response()->download($check->getOriginal()['photo']);
    }

    /**
     * Show add check page.
     *
     * @return \Illuminate\Http\Response
     */
    public function sensFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);
        if ($validator->passes()) {
            /* получатели */
            $to = "promo.bambolina@oasisdrinks.by";
            //$to = "bioforce985@gmail.com";
            $subject = "Сообщение из обратной свзи";

            /* сообщение */
            $message = '
            <html>
                <head>
                    <title>Сообщение из обратной свзи</title>
                </head>
                <body>';
            foreach ($request->except('_token') as $key => $value) {
                $message .= '<p>'.$key.': '.$value.'</p>';
            }
            $message .= '</body></html>';
            $headers= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8\r\n";
            mail($to, $subject, $message, $headers);
            return Response::json(['success' => 1]);
        }
    }

    public function getWinners(Request $request)
    {
        if ($request->id > 0)
        {
            if ($winners = $this->Raffle->find($request->id)->winners()->get())
            {
                $returnWinners = [];
                foreach ($winners as $key => $winner) {
                    $returnWinners[] = [
                        'name' => $winner->check->user->surname.' '.$winner->check->user->name.' '.$winner->check->user->patronymic,
                        'id' => $winner->check->id,
                        'prize' => $winner->prize->name,
                    ];
                    # code...
                }
                return Response::json(['success' => 'Y', 'items' => $returnWinners]);
            }
            return Response::json(['success' => 'N']);
        }
        return Response::json(['success' => 'N']);
    }

    /**
     * Show add check page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCheck(Request $request)
    {
        if (!Auth::check())
            return redirect()->route('home');
        if ($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'year' => 'required',
                'month' => 'required',
                'day' => 'required',
                'hour' => 'required',
                'minute' => 'required',
                'unp' => 'required|max:255',
                'serial' => 'required|max:255',
                //'serial' => 'required|max:255|unique:checks,serial,NULL,id,deleted_at,NULL',
                'photo' => 'required|mimes:jpeg,jpg,png,pdf|max:4096',
            ]);

            if ($validator->passes()) {
                $path = $request->photo->store('check');
                $buy_time = Carbon::create($request->year, $request->month, $request->day, $request->hour, $request->minute, 0, 'Europe/Minsk');
                if ($check = $this->Check->where('unp', $request->unp)->where('serial', $request->serial)->where('buy_time', $buy_time)->first())
                {
                    return back()->withInput($request->all())->withErrors(['photo' => ['Данный чек уже зарегестрирован в системе.']]);
                }
                $checkFields = $request->except(['_token', 'photo', 'year', 'month', 'day', 'hour', 'minute', 'second']);

                $checkFields['photo'] = $path;
                $checkFields['buy_time'] = $buy_time;
                if ($check = Auth::user()->check()->create($checkFields))
                    return back()->with('check_status', true);
                return view('check');
            }
            $validator->validate();
        }
        else
        {
            return view('check');
        }
    }

    public function personalPage()
    {
        return view('personal.index');
    }

    public function userListCheck()
    {
        return view('personal.checks');
    }

    /**
     * Show list of prizes.
     *
     * @return \Illuminate\Http\Response
     */
    public function listPrizes()
    {
        return view('admin.prizes.list', ['prizes' => $this->Prize->paginate(20)]);
    }

    /**
     * Form and add Prize.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPrize(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $request->validate([
                'name' => 'required|unique:prizes',
            ]);
            $this->Prize->create($request->only(['name']));
            return redirect()->route('admin.prizes')->with('message', 'Приз добавлен');
        }
        return view('admin.prizes.add');
    }

    /**
     * Delete Prize.
     *
     * @return redirect to Prize list.
     */
    public function deletePrize(Prize $prize, Request $request)
    {
        $prize->delete();
        return redirect()->route('admin.prizes')->with('message', 'Приз удален');
    }

    /**
     * Show list of raffles.
     *
     * @return \Illuminate\Http\Response
     */
    public function listRaffles()
    {
        return view('admin.raffles.list', ['raffles' => $this->Raffle->paginate(20)]);
    }

    /**
     * Form and add Raffle.
     *
     * @return \Illuminate\Http\Response
     */
    public function addRaffle(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $request->validate([
                'name' => 'required|unique:raffles',
            ]);
            $fields = $request->only(['name', 'sms_date', 'start', 'end']);
            $fields['start'] = Carbon::parse($fields['start']);
            $fields['end'] = Carbon::parse($fields['end']);
            $this->Raffle->create($fields);
            return redirect()->route('admin.raffles')->with('message', 'Розыгрыш добавлен');
        }
        return view('admin.raffles.add');
    }

    /**
     * Delete Raffle.
     *
     * @return redirect to Raffle list.
     */
    public function deleteRaffle(Raffle $raffle, Request $request)
    {
        $raffle->delete();
        return redirect()->route('admin.raffles')->with('message', 'Розыгрыш удален');
    }
    
    /**
     * Activate Raffle.
     *
     * @return redirect to Raffle list.
     */
    public function activateRaffle(Raffle $raffle, Request $request)
    {
        $raffle->active = true;
        $raffle->save();
        return redirect()->route('admin.raffles')->with('message', 'Розыгрыш активен');
    }

    /**
     * Show list of raffle winners.
     *
     * @return \Illuminate\Http\Response
     */
    public function listRaffleWinners(Raffle $raffle, Request $request)
    {
        return view('admin.raffles.winners.list', ['raffle' => $raffle, 'winners' => $raffle->winners()->paginate(10)]);
    }

    /**
     * Form and add raffle winner.
     *
     * @return \Illuminate\Http\Response
     */
    public function addRaffleWinners(Raffle $raffle, Request $request)
    {
        if ($request->isMethod('post'))
        {
            $request->validate([
                'check' => 'required|exists:checks,id,deleted_at,NULL|unique:winners,check_id',
            ]);
            $winner = $raffle->winners()->create(['check_id' => $request->check, 'prize_id' => $request->prize]);
            return redirect()->route('admin.raffle.winners', ['raffle' => $raffle])->with('message', 'Победитель добавлен');
        }
        return view('admin.raffles.winners.add', ['raffle' => $raffle]);
    }




    public function nextCheck()
    {
        $check = $this->Check->where('active', false)->first();
        return view('admin.check', ['check' => $check]);
    }

    public function listCheck(Request $request)
    {
        if ($request->deleted)
        {
            $checks = $this->Check->onlyTrashed()->paginate(10);
        }else{
            $checks = $this->Check->where('active', true)->paginate(10);
        }
        return view('admin.list', ['checks' => $checks]);
    }

    public function exportCheck(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $requestData = $request;
            $job = (new ExportChecks($request->all()))
                ->onConnection('database');
            dispatch($job);
            //Artisan::call('queue:work')->onConnection('database');
            return back();
            if ($request->deleted)
            {
                $data = $this->Check->onlyTrashed()->with('user')->createFrom($request->created_at_from)->createUntil($request->created_at_to)->get();
            }else{
                $data = $this->Check->with('user')->createFrom($request->created_at_from)->createUntil($request->created_at_to)->get();
            }
            
            //$attributes = $data->attributesToArray();
            //$newadta = array_merge($attributes, $data->relationsToArray());
            //->toArray();
            
            $exportData = [];
            $exportData[] = ['id', 'УНП', 'Время покупки', 'Номер', 'Загружено', 'Имя', 'Фамилия', 'Отчество', 'Телефон', 'Адрес'];
            foreach($data as $row)
            {
                //echo "<pre>"; print_r(array_merge($row->attributesToArray(), $row->relationsToArray())); echo "</pre>";
                //$row->attributesToArray();
                $newRow = array_only($row->toArray(), ['id', 'unp', 'serial', 'buy_time', 'created_at']);
                
                //echo "<pre>"; print_r($newRow); echo "</pre>";
                //return "1";
                //unset($newRow['user']);
                $newRow['name'] = $row->user->name;
                $newRow['surname'] = $row->user->surname;
                $newRow['patronymic'] = $row->user->patronymic;
                $newRow['phone'] = $row->user->login;
                $newRow['addres'] = $row->user->addres;
                
                $exportData[] = $newRow;
            }
            //echo "<pre>"; print_r($exportData); echo "</pre>";
            //return "1";
            //(new Collection($exportData))->downloadExcel('export', 'Xls');
            Excel::create('export', function($excel) use($exportData) {

                $excel->sheet('Sheetname', function($sheet) use($exportData) {

                    $sheet->fromArray($exportData);

                });

            })->download('xls');
        }
        else
            return view('admin.export');
    }

    public function getExports(Request $request)
    {
        if ($request->isMethod('post'))
        {
            return response()->download(storage_path('app/excel/exports/').$request->file);
            //print_r($request->all());
            return "1";
        }
        $files = File::files(storage_path('app/excel/exports'));
        $fileOne = $files[0];
        //echo pathinfo($fileOne)['filename'];
        //return response()->download($fileOne);
        //echo "<pre>"; print_r(pathinfo($fileOne)); echo "</pre>";
        foreach ($files as $file)
        {
            //echo pathinfo($fileOne)['basename'], "<br/>";
        }
        return view('admin.export_files', ['files' => $files]);
        return "1";
    }

    public function updateCheck(Check $check, Request $request)
    {
        if ($request->type == 'approve')
        {
            $check->approve($request->col);
            return back();
        }
        if ($request->type == 'decline')
        {
            if (!$request->reason)
            {
                return back()->withInput($request->all())->withErrors(['reason' => ['Введите причину отклонения']]);
            }
            $check->decline($request->reason);
            return back();
        }
        return back();
    }

    public function sendOldSMS(Request $request)
    {
        if ($request->isMethod('post'))
        {
            if (!$request->id)
            {
                return view('admin.sendsms');
            }
            if (!$request->count)
            {
                return view('admin.sendsms');
            }
            $check = $this->Check->where('active', true)->where('id', $request->id)->first();
            if ($check)
            {
                //echo "<pre>"; print_r($check); echo "</pre>";
                $check->sendApprovedSms($request->count);
            }
            return redirect()->route('sendsms')->with('message', 'Чеки добавлены');
        }
        else
        {
            return view('admin.sendsms');
        }
    }

}
