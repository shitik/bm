<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    //
    protected $fillable = [
        'name',
    ];

    public function winners()
    {
        return $this->hasMany('App\Winner'); 
    }
}
