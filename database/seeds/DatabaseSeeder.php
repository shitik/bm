<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Admin',
            'surname' => 'Admin',
            'patronymic' => 'Admin',
            'addres' => str_random(10),
            'login' => 'admin',
            'email' => 'admin@admin.by',
            'password' => bcrypt('123456'),
        ]);
    }
}
