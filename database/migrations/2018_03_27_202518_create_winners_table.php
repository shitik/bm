<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('check_id')->unsigned();
            $table->integer('prize_id')->unsigned();
            $table->integer('raffle_id')->unsigned();
            $table->timestamps();

            $table->foreign('check_id')->references('id')->on('checks')->onDelete('cascade');;
            $table->foreign('prize_id')->references('id')->on('prizes')->onDelete('cascade');;
            $table->foreign('raffle_id')->references('id')->on('raffles')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners');
    }
}
