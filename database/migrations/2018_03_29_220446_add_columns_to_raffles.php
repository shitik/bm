<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRaffles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raffles', function (Blueprint $table) {
            //
            $table->timestamp('end')->nullable()->after('name');
            $table->timestamp('start')->nullable()->after('name');
            $table->string('sms_date')->useCurrent()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raffles', function (Blueprint $table) {
            //
            $table->dropColumn('start');
            $table->dropColumn('end');
            $table->dropColumn('sms_date');
        });
    }
}
