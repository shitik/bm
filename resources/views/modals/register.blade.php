<div id="RegisterModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Sign Up</h3>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <div id="success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> You are login now!
                    </div>
                </div>
                <div id="success-msg-register" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> Check your phone for login confirmation!!
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10 hide" id="confirm-register-code">
                    <form method="POST" id="ConfirmRegister">
                        {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <input type="text" name="code" value="{{ old('code') }}" class="form-control" placeholder="Введите код из смс">
                            <span class="text-danger">
                                <strong id="code-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                              <button type="button" id="confirmForm" class="btn btn-primary btn-prime white btn-flat">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-offset-1 col-md-10" id="register-form">
                    <form method="POST" id="Register">
                        {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Имя">
                            <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" name="surname" value="{{ old('surname') }}" class="form-control" placeholder="Фамилия">
                            <span class="text-danger">
                                <strong id="surname-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" name="patronymic" value="{{ old('patronymic') }}" class="form-control" placeholder="Отчество">
                            <span class="text-danger">
                                <strong id="patronymic-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" name="addres" value="{{ old('addres') }}" class="form-control" placeholder="Адрес регистрации">
                            <span class="text-danger">
                                <strong id="addres-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Электронная почта">
                            <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" name="login" value="{{ old('login') }}" class="form-control" placeholder="Номер мобильного телефона">
                            <span class="text-danger">
                                <strong id="login-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                              <button type="button" id="submitForm" class="btn btn-primary btn-prime white btn-flat">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>