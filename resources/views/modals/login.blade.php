<div id="SignUp" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title text-center primecolor">Sign Up</h3>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <div id="login-success-msg" class="hide">
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      <strong>Success!</strong> You are login now!
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10 hide" id="confirm-login-code">
                    <form method="POST" id="ConfirmLogin">
                        {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <input type="text" name="code" value="{{ old('code') }}" class="form-control" placeholder="Введите код из смс">
                            <span class="text-danger">
                                <strong id="login-code-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                              <button type="button" id="confirmLoginForm" class="btn btn-primary btn-prime white btn-flat">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-offset-1 col-md-10" id="login-form">
                    <form method="POST" id="Login">
                        {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <input type="text" name="login" value="{{ old('login') }}" class="form-control" placeholder="Телефон">
                            <span class="text-danger">
                                <strong id="login-login-error"></strong>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                              <button type="button" id="submitLoginForm" class="btn btn-primary btn-prime white btn-flat">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>