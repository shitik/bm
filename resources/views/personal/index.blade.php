@extends('layouts.app')

@section('content')
<div role="main" class="main">
    <div class="row full regi-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-12 regi-head">
                <p class="head">Мой кабинет</p>
                <a href="{{url('/logout')}}" class="exit">( Выход )</a>
            </div>
            <div class="col-md-12 buttons">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <a href="{{route('user.add.check')}}">Загрузить чеки</a>
                </div>
                <div class="col-md-3">
                    <a href="{{route('user.list.check')}}">Загруженные чеки</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('body_class', 'cabinet')