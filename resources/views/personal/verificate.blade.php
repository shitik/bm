@extends('layouts.app')

@section('content')
<div role="main" class="main">
    <div class="row full regi-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-12 regi-head">
                <p class="head">Регистрация</p>
            </div>
            <div class="col-md-12">
                <form id="regiForm" class="regiForm" action="{{ url('/verificate') }}" method="POST" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Введите код из SMS</label>
                                <input type="text" value="{{ old('code') }}" name="code" id="code">
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <input type="submit" value="Подтвердить" class="btn-reg">
                        </div>
                    </div>
                    <input type="hidden" name="activate" value='Y'>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('body_class', 'mychecks')