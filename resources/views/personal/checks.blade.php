@extends('layouts.app')

@section('content')
<div role="main" class="main">
    <div class="row full regi-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-12 regi-head">
                <p class="head">Мои чеки</p>
            </div>
            <div class="col-md-12">
                <table>
                    <thead>
                        <th>Код участника</th>
                        <th>Дата регистрации</th>
                        <th>Статус</th>
                    </thead>
                    <tbody>
                        @foreach (Auth::user()->check()->withTrashed()->get() as $check)
                        <tr>
                            <td>{{$check->id}}</td>
                            <td>{{$check->created_at}}</td>
                            @if($check->trashed())
                            <td class="notapproved">Отклонен@if($check->reason)<br/>({{$check->reason}})@endif</td>
                            @elseif($check->active)
                            <td class="approved">Подтвержден</td>
                            @else
                            <td>В обработке</td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('body_class', 'mychecks')