<!DOCTYPE html>
<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PM3FXN4');</script>
    <!-- End Google Tag Manager -->
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   

    <title>Рекламная игра BAMBOLINA – «играй с BAMBOLINA»</title>  

    <meta name="keywords" content="Bambolina" />
    <meta name="description" content="Рекламная игра BAMBOLINA – «играй с BAMBOLINA»">
    <meta name="author" content="bambolina">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon" />

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- JS CSS -->
    <link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/js/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="/js/bootstrap-datepicker/bootstrap-datepicker3.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/style.css?v=1.5">

</head>
<body class="@yield('body_class', 'home')">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PM3FXN4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header id="header">    
        <div class="header-row">
            <div class="header-logo">
                <a href="http://bambolina.by">
                    <img alt="Bambolina" src="/img/logo.png">
                </a>
            </div>
            <div class="header-nav">
                <nav>
                    <ul class="nav">
                        <li>
                            <a href="@yield('home_link', '/')#rules" class="scroll-link">Условия участия</a>
                        </li>
                        <li class="">
                            <a href="@yield('home_link', '/')#prizes" class="scroll-link">Призы</a>
                        </li>
                        <?/*
                        @guest
                        <li>
                            <a class="popup-with-form noscroll" href="#entranceForm">Регистрация чека</a>
                        </li>
                        @else
                        <li>
                            <a class="noscroll" href="{{route('personal')}}">Личный кабинет</a>
                        </li>
                        @endif
                        */?>
                        <li>
                            <a href="@yield('home_link', '/')#winners" class="scroll-link">Победители</a>
                        </li>
                    </ul>
                </nav>      
            </div>
            <div class="header-tel">
                <p>+375 44 514 57 75</p>        
            </div>
        </div>
    </header>
    @yield('content')
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-nav">
                        <nav>
                            <ul class="nav">
                                <li>
                                    <a href="@yield('home_link', '/')#rules" class="scroll-link">Условия участия</a>
                                </li>
                                <li>
                                    <a href="/rules.pdf" target="_blank">Полные правила игры</a>
                                </li>
                                <li class="">
                                    <a href="@yield('home_link', '/')#prizes" class="scroll-link">Призы</a>
                                </li>
                                <?/*
                                <li>
                                    <a class="popup-with-form noscroll" href="#entranceForm">Регистрация чека</a>
                                </li>
                                */?>
                                <li>
                                    <a href="@yield('home_link', '/')#winners" class="scroll-link">Победители</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-descr">
                        <p>Рекламная игра "Играй с BAMBOLINA".</p>
                        <p>Свидетельство о государственной регистрации рекламной игры от 06.03.18г №3221. Выдано МАРТ Республики Беларусь.</p>
                        <p>Организатор ООО "Минт филд", УНП 192465627. Период проведения рекламной игры с 1 апреля по 20 июля 2018 года.</p>
                        <p>Срок регистрации чеков для участия в рекламной игре с 1 апреля по 29 июня 2018 года.</p>
                    </div>
                    <div class="footer-descr-contact">
                        <a class="popup-with-form" href="#contactForm">Свяжитесь с нами</a>
                        <form id="contactForm" action="" method="POST" class="white-popup-block obl-cf mfp-hide">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Ваше имя</label>
                                        <input type="text" value="" name="name" id="name" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Ваша фамилия</label>
                                        <input type="text" value="" name="surname" id="surname">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Ваше телефон</label>
                                        <input type="tel" value="" name="telephone" id="telephone" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Ваш email</label>
                                        <input type="email" value="" name="email" id="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Сообщение</label>
                                        <textarea rows="4" name="message" id="message" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Отправить" class="btn-cf">
                                </div>
                            </div>
                            <div class="alert alert-success hidden mt-lg" id="contactSuccess">
                                Ваше сообщение отправлено.
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-contacts">
                        <p>Телефон горячей линии: +375 44 514 57 75</p>
                        <p>Время работы горячей линии: с 1 апреля по 15 июля 2018 года</p>
                        <p>с 13.00ч до 16.00ч по будним дням</p>
                        <p>с 10.00ч до 12.00ч по выходным</p>
                    </div>
                    <div class="footer-contacts-f16">
                        <p>Разработка сайта - <a href="http://factory16.by"><img src="/img/logof16.png"></a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?/*
    @guest
    <div id="entranceForm" class="white-popup-block obl-entrance mfp-hide">
        <div id="entrance">
            <a href="{{url('/register')}}">Регистрация нового участника</a>
            <a id="password" href="#">Вход с помощью пароля</a> 
        </div>
    </div>
    <form id="smsForm" action="" method="POST" novalidate="novalidate" class="white-popup-block obl-sms mfp-hide">
        <label>Введите код из смс</label>
        <div id="login-code-error" class="alert alert-danger hide"></div>
        <input type="text" value="" name="sms" id="sms">
        <input type="submit" value="Далее" class="btn-sms">
    </form>
    <form id="teleForm" action="" method="POST" novalidate="novalidate" class="white-popup-block obl-tele mfp-hide">
        <p class="tele-header">Вход в личный кабинет</p>
        <label>Телефон</label>
        <div id="login-login-error" class="alert alert-danger hide"></div>
        <input type="tel" name="tele" id="phone" placeholder="+375XXXXXXXXX">
        <p class="tele-footer">Войдите в личный кабинет, используя пароль, отправленный СМС - сообщением на указанный номер телефона.</p>
        <input type="submit" value="Далее" class="btn-tele">
    </form>
    @else
    <div id="finishForm" class="white-popup-block obl-finish mfp-hide">
        <p>Ваш чек загружен и принят на рассмотрение.<br>Хотите зарегистрировать еще чек?</p>
        <div id="finish">
            <a href="{{route('user.add.check')}}">Да</a>
            <a href="{{url('/')}}">Нет</a>    
        </div>
    </div>
    @endauth
    */?>
    <!-- JS -->
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="/js/jquery.validation/jquery.validation.min.js"></script>
    <script src="/js/custom.js?v=1.3"></script>
    @hasSection('adittional-scripts')
        @yield('adittional-scripts')
    @endif
    @if (session('check_status'))
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $.magnificPopup.open({
                items: {
                    src: '#finishForm'
                },
                type: 'inline'
            });
        });
    </script>
    <script type="text/javascript">
        dataLayer.push({
          'event' : 'event-to-ga',
          'eventCategory' : 'Send',
          'eventAction' : 'Form'
        });
    </script>>
    @endif
</body>
</html>