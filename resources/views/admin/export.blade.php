@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Export</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('check.export', Request::only('deleted')) }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="created_at_to" class="col-md-4 control-label">Название файла</label>

                            <div class='col-md-6 input-group'>
                                <input type='text' class="form-control input" name="name" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="created_at_from" class="col-md-4 control-label">Дата от</label>

                            <div class='col-md-6 input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="created_at_from" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="created_at_to" class="col-md-4 control-label">Дата до</label>

                            <div class='col-md-6 input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="created_at_to" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
