@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if (!$check)
                <div class="panel-heading">Проверка чека</div>
                <div class="panel-body">
                    Нету чеков для проверки
                </div>
                @else
                <div class="panel-heading">Чек № {{$check->id}}</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('check.update', ['check' => $check]) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-5 text-right">УНП продавца</label>
                            <div class="col-md-6">{{$check->unp}}</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 text-right">Дата и время в чеке</label>
                            <div class="col-md-6"><span>{{$check->buy_time}}</span></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 text-right">Серийный номер чека</label>
                            <div class="col-md-6">{{$check->serial}}</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 text-right">Фото кассового чека</label>
                            @if ($check->image)
                            <div class="col-md-6"><a href="#" data-toggle="modal" data-target="#PhotoCheck"><img src="{{$check->photo}}" class="img-thumbnail"></a></div>
                            @else
                            <div class="col-md-6"><a href="{{route('admin.view.check', ['check' => $check])}}" target="_blank">PDF</a></div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 text-right">Количество присваиваемых кодов</label>
                            <div class="col-md-6"><input type="number" name="col" min="1" value="{{old('col', 1)}}" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10 text-left">
                                <button type="submit" name="type" value="approve" class="btn btn-success btn-lg" role="button" aria-pressed="true">Утвердить</button>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                            <div class="col-md-2"></div>
                            <div class="col-md-10 text-left">
                                <input type="text" name="reason" placeholder="Причина отклонения" class="form-control">
                                @if ($errors->has('reason'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reason') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10 text-left">
                                <button type="submit" name="type" value="decline" class="btn btn-danger btn-lg" role="button" aria-pressed="true">Отклонить</button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@if ($check)
@push('modals')
<div id="PhotoCheck" class="modal">
    <span class="close" data-dismiss="modal">&times;</span>

    <!-- Modal Content (The Image) -->
    <img class="modal-content" id="img01" src="{{$check->photo}}">

</div>
@endpush
@endif