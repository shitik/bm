@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    &nbsp;
                    <li>
                        <a href="{{ route('check.export', Request::except('page')) }}">
                            Выгрузить
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="nav-item nav-link{{(!Request::get('deleted')) ? ' open' : ''}}">
                        <a href="{{route('check.list', Request::except('deleted'))}}" class="nav-link">Активные чеки</a>
                    </li>
                    <li class="nav-item nav-link{{(Request::get('deleted')) ? ' open' : ''}}">
                        <a href="{{route('check.list', array_merge(Request::all(), ['deleted' => 'y']))}}" class="nav-link">Отклоненные чеки</a>
                    </li>
                    <li class="nav-item nav-link">
                        <a href="{{route('check.export.files')}}" class="nav-link">Выгруженные чеки</a>
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        @if ($files)
                        @foreach ($files as $file)
                        <tbody>
                        <tr>
                        <th scope="row">{{pathinfo($file)['filename']}}</th>
                        <td>
                            <form method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="file" value="{{pathinfo($file)['basename']}}">
                                <input type="submit" name="tyep" value="Download">
                            </form>
                        </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection