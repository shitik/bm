@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Export</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('sendsms') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="created_at_to" class="col-md-4 control-label">ID</label>

                            <div class='col-md-6 input-group'>
                                <input type='text' class="form-control input" name="id" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="created_at_to" class="col-md-4 control-label">Количество</label>

                            <div class='col-md-6 input-group'>
                                <input type='text' class="form-control input" name="count" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send sms
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
