@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-heading">Добавить розыгрыш</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('admin.raffles.add') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Название</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('sms_date') ? ' has-error' : '' }}">
                            <label for="sms_date" class="col-md-4 control-label">SMS</label>

                            <div class="col-md-6">
                                <input id="sms_date" type="text" class="form-control" name="sms_date" value="{{ old('sms_date') }}" required autofocus>

                                @if ($errors->has('sms_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sms_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('start') ? ' has-error' : '' }}">
                            <label for="start" class="col-md-4 control-label">Начало</label>

                            <div class="col-md-6 input-group date" id='datetimepicker1'>
                                <input id="start" type="text" class="form-control" name="start" value="{{ old('start') }}" required autofocus>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @if ($errors->has('start'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('end') ? ' has-error' : '' }}" id='datetimepicker2'>
                            <label for="end" class="col-md-4 control-label">Окончание</label>

                            <div class="col-md-6 input-group date">
                                <input id="end" type="text" class="form-control" name="end" value="{{ old('end') }}" required autofocus>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                @if ($errors->has('end'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-6 text-right">
                                <button type="submit" name="type" value="add" class="btn btn-success btn-lg" role="button" aria-pressed="true">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection