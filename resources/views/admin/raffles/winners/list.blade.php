@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    &nbsp;
                    <li>
                        <a href="{{ route('admin.raffle.winners.add', ['raffle' => $raffle]) }}">
                            Добавить победителя
                        </a>
                    </li>
                </ul>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Имя</th>
                                <th scope="col">Телефон</th>
                                <th scope="col">Приз</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        @if ($winners)
                        @foreach ($winners as $winner)
                        <tbody>
                        <tr>
                        <th scope="row">{{$winner->id}}</th>
                        <td>{{$winner->check->user->name}}</td>
                        <td>{{$winner->check->user->login}}</td>
                        <td>{{$winner->prize->name}}</td>
                        <?/*
                        <td>
                            <a href="{{route('admin.raffle.delete', ['raffle' => $raffle])}}">Удалить</a>  |   
                            <a href="{{route('admin.raffle.activate', ['raffle' => $raffle])}}">Активировать</a>  |   
                            <a href="{{route('admin.raffle.winners', ['raffle' => $raffle])}}">Победители</a>
                        </td>
                        */?>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{$winners->appends(Request::input())->links()}}
        </div>
    </div>
</div>
@endsection