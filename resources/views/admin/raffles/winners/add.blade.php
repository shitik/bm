@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-heading">Добавить победителя для розыгрыша: </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('admin.raffle.winners.add', ['raffle' => $raffle]) }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('check') ? ' has-error' : '' }}">
                            <label for="check" class="col-md-4 control-label">ID чека</label>

                            <div class="col-md-6">
                                <input id="check" type="text" class="form-control" name="check" value="{{ old('check') }}" required autofocus>

                                @if ($errors->has('check'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('check') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('prize') ? ' has-error' : '' }}">
                            <label for="prize" class="col-md-4 control-label">Приз</label>

                            <div class="col-md-6">
                                <select class="form-control" name="prize">
                                    @foreach(App\Prize::all() as $prize)
                                    <option value="{{$prize->id}}">{{$prize->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('prize'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prize') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-6 text-right">
                                <button type="submit" name="type" value="add" class="btn btn-success btn-lg" role="button" aria-pressed="true">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection