@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    &nbsp;
                    <li>
                        <a href="{{ route('admin.raffles.add') }}">
                            Добавить розыгрыш
                        </a>
                    </li>
                </ul>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        @if ($raffles)
                        @foreach ($raffles as $raffle)
                        <tbody>
                        <tr>
                        <th scope="row">{{$raffle->id}}</th>
                        <td>{{$raffle->name}}</td>
                        <td>
                            <a href="{{route('admin.raffle.delete', ['raffle' => $raffle])}}">Удалить</a>  |   
                            <a href="{{route('admin.raffle.activate', ['raffle' => $raffle])}}">Активировать</a>  |   
                            <a href="{{route('admin.raffle.winners', ['raffle' => $raffle])}}">Победители</a>
                        </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{$raffles->appends(Request::input())->links()}}
        </div>
    </div>
</div>
@endsection