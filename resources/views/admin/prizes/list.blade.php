@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    &nbsp;
                    <li>
                        <a href="{{ route('admin.prizes.add') }}">
                            Добавить приз
                        </a>
                    </li>
                </ul>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        @if ($prizes)
                        @foreach ($prizes as $prize)
                        <tbody>
                        <tr>
                        <th scope="row">{{$prize->id}}</th>
                        <td>{{$prize->name}}</td>
                        <td><a href="{{route('admin.prizes.delete', ['prize' => $prize])}}">Удалить</a></td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{$prizes->appends(Request::input())->links()}}
        </div>
    </div>
</div>
@endsection