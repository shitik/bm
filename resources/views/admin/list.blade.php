@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    &nbsp;
                    <li>
                        <a href="{{ route('check.export', Request::except('page')) }}">
                            Выгрузить
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="nav-item nav-link{{(!Request::get('deleted')) ? ' open' : ''}}">
                        <a href="{{route('check.list', Request::except('deleted'))}}" class="nav-link">Активные чеки</a>
                    </li>
                    <li class="nav-item nav-link{{(Request::get('deleted')) ? ' open' : ''}}">
                        <a href="{{route('check.list', array_merge(Request::all(), ['deleted' => 'y']))}}" class="nav-link">Отклоненные чеки</a>
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">УНП продавца</th>
                                <th scope="col">Дата</th>
                                <th scope="col">Серийный номер</th>
                                <th scope="col">Фото</th>
                                <th scope="col">Дата добавления</th>
                            </tr>
                        </thead>
                        @if ($checks)
                        @foreach ($checks as $check)
                        <tbody>
                        <tr>
                        <th scope="row">{{$check->id}}</th>
                        <td>{{$check->unp}}</td>
                        <td>{{$check->buy_time}}</td>
                        <td>{{$check->serial}}</td>
                        @if ($check->image)
                        <td><img src="{{$check->photo}}" class="img-thumbnail" style="max-width:80px;"></td>
                        @else
                        <td><a href="{{route('admin.view.check', ['check' => $check])}}" target="_blank">PDF</a></td>
                        @endif                        
                        <td>
                            {{$check->created_at}}
                            @if ($check->trashed() && $check->reason)
                            <br/>Причина: {{$check->reason}}
                            @endif
                        </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
            {{$checks->appends(Request::input())->links()}}
        </div>
    </div>
</div>
@endsection