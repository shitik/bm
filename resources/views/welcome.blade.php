@extends('layouts.app')

@section('content')
<div role="main" class="main">
    <div class="row full main-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-6">
                <div class="cloud1">
                    <p>с <span class="font-up">1 апреля</span><br>по <span class="font-up">29 июня</span></p>
                </div>
                <div class="cloud2">
                    <p>купи 4 любых пюре<br>BAMBOLINA в одном чеке и участвуй<br>в розыгрыше призов.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="sheep noscroll"><p>Регистрация чеков завершена</p></div>
                <?/*
                @guest
                <a class="sheep popup-with-form noscroll" href="#entranceForm"><p>зарегистрируйте чек</p></a>
                @else
                <a class="sheep noscroll" href="{{route('user.add.check')}}"><p>зарегистрируйте чек</p></a>
                @endif
                */?>
            </div>
        </div>
        <div class="container">
            <div class="col-md-6">
                <img class="banki" src="/img/banki.png">
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12">
            <div class="field1" id="rules">
                <p>условия участия</p>
            </div>
        </div>
    </div>

    <div class="row full prizes-row">
        <div class="container">
            <div class="col-md-4 bottles-holder">
                <p class="title">Покупай</p>
                <div class="bottles">
                    <img src="/img/bottles.png">
                </div>
                <p class="description">4 любые баночки пюре<br>Bambolina в одном чеке</p>
                <div class="rules">
                    <a href="/rules.pdf" target="_blank">Полные правила<br>рекламной игры</a>
                </div>
            </div>
            <div class="col-md-4 chek-holder">
                <div class="plane-top"></div>
                <p class="title">Регистрируй<br><span>чек *</span></p>
                <div class="chek">
                    <img src="/img/chek.png">
                </div>
                <p class="description" style="margin-top:  30px">*Период покупки и регистрации чеков с 01.04.2018 по 29.06.2018.</p>
                <p class="description">Зарегистрированные чеки необходимо сохранить до окончания рекламной игры.</p>
                <div class="plane-bottom"></div>
            </div>
            <div class="col-md-4">
                <p class="title" style="text-align: center">Выигрывай призы<span>от Bambolina:</span></p>
                <div class="toys">
                    <img src="/img/toys.png">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12">
            <div class="field2" id="prizes">
                <p>призы</p>
            </div>
        </div>
    </div>

    <div class="row full winners-row">
        <div class="container">
            <div class="col-md-4 money-holder">
                <div class="money">
                    <p>Денежные призы</p>
                    <p class="res">100 BYN <span class="number">x 180</span></p>
                </div>
                <p class="subtitle">Розыгрыши призов<br>проводятся еженедельно !</p>
            </div>
            <div class="col-md-4 zveri-holder">
                <div class="zveri">
                    <img src="/img/zveri.png">
                </div>
                <div class="zveri-priz">
                    <p>Мягкие игрушки<br><span style="display: inline-block; margin-left: -20px;">ручной</span><br><span class="left">работы</span></p>
                    <p class="res">x <span class="number">240</span></p>
                </div>
            </div>
            <div class="col-md-4 food-holder">
                <p class="subtitle smaller">Розыгрыш главного приза 29.06.2018</p>
                <div class="food">
                    <p>Главный приз:<span>"365 обедов</span><span>с Bambolina"</span><span>и полная коллекция</span><span>игрушек</span></p> <p class="res">x <span class="number">1</span></p>
                </div>
                <p class="description">* «365 обедов с Bambolina» включает в себя комплект детского питания Bambolina в ассортименте: фруктовое пюре 372 баночки, овощное пюре 360 баночек, мясные консервы 96 баночек, рыбные консервы 96 баночек, соки 162 пачки по 200 мл., нектары 216 пачек по 200 мл.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12">
            <div class="field3" id="winners">
                <p>победители</p>
            </div>
        </div>
    </div>
    @if (App\Raffle::count())
    <div class="container results">
        <div class="col-md-12">
            <div class="youtbs">
                <div class="youtube">
                    <p>посмотреть<br>розыгрыши призов<br>можно<a href="https://www.youtube.com/channel/UCjutLAAfrXXTSA33PB-_wRQ" target="_blank">здесь</a></p>
                </div>
                <div class="youtube winrs">
                    <p>посмотреть<br>список победителей<br>можно<a href="/winners.pdf" target="_blank">здесь</a></p>
                </div>
            </div>
            <div class="winners">
                <form method="post">
                    <label class="date-label" for="date">Выберите дату розыгрыша</label>
                    <div class="select-style" style="width: 220px">
                        <select name="winners-date" id="winners-date">
                            @foreach (App\Raffle::active()->get() as $raffle)
                            <option value="{{$raffle->id}}" @if ($raffle->id == App\Raffle::getActiveId()) selected @endif>{{$raffle->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>

                <table>
                    <thead>
                        <th>ФИО</th>
                        <th>Код участника</th>
                        <th>Приз</th>
                    </thead>
                    <tbody>
                        @foreach (App\Raffle::find(App\Raffle::getActiveId())->winners()->get() as $winner)
                        <tr>
                            <td>{{$winner->check->user->surname}} {{$winner->check->user->name}} {{$winner->check->user->patronymic}}</td>
                            <td>{{$winner->check->id}}</td>
                            <td>{{$winner->prize->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@section('home_link', '')
@section('body_class', 'home')