@extends('layouts.app')

@section('content')

<div role="main" class="main">
    <div class="row full regi-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-12 regi-head">
                <p class="head">Регистрация</p>
            </div>
            <div class="col-md-12">
                <form id="regiForm" class="regiForm" action="{{ route('register') }}" method="POST" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Фамилия</label>
                                <input type="text" value="{{ old('surname') }}" name="surname" id="surname">
                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label>Имя</label>
                                <input type="text" value="{{ old('name') }}" name="name" id="name">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label>Отчество</label>
                                <input type="text" value="{{ old('patronymic') }}" name="patronymic" id="patronymic">
                                @if ($errors->has('patronymic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('patronymic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="email" value="{{ old('email') }}" name="email" id="email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label>Телефон</label>
                                <input type="telephone" value="{{ old('login') }}" name="login" id="regPhone" pattern="^\+375(\s+)?\(?(17|25|29|33|44)\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$">
                                @if ($errors->has('login'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Индекс</label>
                                <input type="text" value="{{ old('index') }}" name="index" id="index">
                                @if ($errors->has('index'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('index') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label>Населенный пункт</label>
                                <input type="text" value="{{ old('city') }}" name="city" id="city">
                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Улица</label>
                                <input type="text" value="{{ old('street') }}" name="street" id="street">
                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <label>Дом</label>
                                <input type="text" value="{{ old('building') }}" name="building" id="building">
                                @if ($errors->has('building'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('building') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <label>Корпус</label>
                                <input type="text" value="{{ old('corpus') }}" name="corpus" id="corpus">
                            </div>
                            <div class="col-md-2">
                                <label>Квартира</label>
                                <input type="text" value="{{ old('appart') }}" name="appart" id="appart">
                                @if ($errors->has('appart'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('appart') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-12 agree">
                            <div class="col-md-6">
                                Я принимаю условия хранения и обработки моих персональных данных
                                @if ($errors->has('persData'))
                                    <span class="help-block">
                                        <strong>Вы должны принять условия</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" value="Y" name="persData" id="agreePersData">
                                <span class="checkmark"></span>                                
                            </div>
                        </label>
                    </div>
                    <div class="row">
                        <label class="col-md-12 agree">
                            <div class="col-md-6">
                                Я согласен и ознакомлен с настоящими Правилами рекламной игры
                                @if ($errors->has('rules'))
                                    <span class="help-block">
                                        <strong>Вы должны согласиться с Правилами</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" value="Y" name="rules" id="agreeRules">
                                <span class="checkmark"></span>                                
                            </div>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-12 risunok">
                            <img src="/img/risunok.png">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align: center">
                            <input type="submit" value="Зарегистрироваться" class="btn-reg">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
