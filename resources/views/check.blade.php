@extends('layouts.app')

@section('content')
<div role="main" class="main">
    <div class="row full regi-row">
        <div class="sky"></div>
        <div class="container">
            <div class="col-md-12 regi-head">
                <p class="head">Регистрация чека</p>
            </div>
            <div class="col-md-12">
                <form id="checkForm" class="regiForm" action="{{ route('user.add.check') }}" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6 nopads">
                                <label style="padding: 0px 15px">Дата покупки</label>
                                <div class="col-md-4{{ $errors->has('day') ? ' has-error' : '' }}">
                                    <div class="select-style">
                                        <select name="day" id="day">
                                            @for($i=1; $i<=31; $i++)
                                            <option value="{{$i}}" {{old('day') == $i ? 'selected' : ''}}>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    @if ($errors->has('day'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('day') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4{{ $errors->has('month') ? ' has-error' : '' }}">
                                    <div class="select-style">
                                        <select name="month" id="month">
                                            <option value="4" {{old('month') == 4 ? 'selected' : ''}}>апрель</option>
                                            <option value="5" {{old('month') == 5 ? 'selected' : ''}}>май</option>
                                            <option value="6" {{old('month') == 6 ? 'selected' : ''}}>июнь</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('month'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('month') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4{{ $errors->has('year') ? ' has-error' : '' }}">
                                    <div class="select-style">
                                        <select name="year" id="year">
                                            <option value="2018" selected>2018</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('year'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 nopads">
                                <label style="padding: 0px 15px">Время покупки</label>
                                <div class="col-md-6{{ $errors->has('hour') ? ' has-error' : '' }}">
                                    <div class="select-style">
                                        <select name="hour" id="hour">
                                            @for($i=0; $i<=9; $i++)
                                            <option value="{{$i}}" {{old('hour') == $i ? 'selected' : ''}}>0{{$i}} Ч</option>
                                            @endfor
                                            @for($i=10; $i<=23; $i++)
                                            <option value="{{$i}}" {{old('hour') == $i ? 'selected' : ''}}>{{$i}} Ч</option>
                                            @endfor
                                        </select>
                                    </div>
                                    @if ($errors->has('hour'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('hour') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6{{ $errors->has('minute') ? ' has-error' : '' }}">
                                    <div class="select-style">
                                        <select name="minute" id="minute">
                                            @for($i=0; $i<=9; $i++)
                                            <option value="{{$i}}" {{old('minute') == $i ? 'selected' : ''}}>0{{$i}} МИН</option>
                                            @endfor
                                            @for($i=10; $i<=59; $i++)
                                            <option value="{{$i}}" {{old('minute') == $i ? 'selected' : ''}}>{{$i}} МИН</option>
                                            @endfor
                                        </select>
                                    </div>
                                    @if ($errors->has('minute'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('minute') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6{{ $errors->has('unp') ? ' has-error' : '' }}">
                                <label>УНП продавца</label>
                                <input type="text" value="{{ old('unp') }}" name="unp" id="unp">
                                @if ($errors->has('unp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('unp') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6{{ $errors->has('serial') ? ' has-error' : '' }}">
                                <label>Номер чека</label>
                                <input type="text" value="{{old('serial')}}" name="serial" id="checknum">
                                @if ($errors->has('serial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('serial') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6{{ $errors->has('photo') ? ' has-error' : '' }}">
                                <label>Загрузить чек</label>
                                <input type="file" name="photo" accept=".gif,.png,.jpg,.jpeg,.pdf" value="{{old('photo')}}">
                                @if ($errors->has('photo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif
                                <p class="descr-text">допустимый размер до 4 Мб</p>
                                <p class="descr-text">допустимые форматы файла изображения – jpg, png, gif, pdf</p>
                                <div class="col-md-12" style="text-align: center">
                                    <input type="submit" value="Зарегистрировать чек" class="btn-reg reg-check">
                                </div>
                            </div>
                            <div class="col-md-6 check-head">
                                <p class="head">Пример чека</p>
                                <div class="zoom-holder">
                                    <img src="/img/zoom.png">
                                </div>
                                <img src="/img/check.png" class="check-pic">
                                <img src="/img/risunok.png" class="abs-ris">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('body_class', 'registration')
@section('adittional-scripts')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        /*for (var i = 0; i <= 9; i++) {
            $('#hour').append('<option value="' + i + '">0' + i + ' Ч</option>');
            $('#minute').append('<option value="' + i + '">0' + i + ' МИН</option>');
        }
        for (var i = 10; i <= 23; i++) {
            $('#hour').append('<option value="' + i + '">' + i + ' Ч</option>');
        }
        for (var i = 10; i <= 59; i++) {
            $('#minute').append('<option value="' + i + '">' + i + ' МИН</option>');
        }
        for (var i = 1; i <= 31; i++) {
            $('#day').append('<option value="' + i + '">' + i + '</option>');
        }*/
    });
</script>
@endsection

