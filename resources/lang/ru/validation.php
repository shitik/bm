<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => '":attribute" должен быть одобрен.',// The :attribute must be accepted.
    'active_url'           => 'Ссылка ":attribute" неверная.',// The :attribute is not a valid URL.
    'after'                => 'Дата ":attribute" должна быть после :date.',// The :attribute must be a date after :date.
    'alpha'                => 'Поле ":attribute" должно содержать только буквы.',// The :attribute may only contain letters.
    'alpha_dash'           => 'Поле ":attribute" должно содержать только буквы, цифры и тире.',// The :attribute may only contain letters, numbers, and dashes.
    'alpha_num'            => 'Поле ":attribute" должно содержать только буквы и цифры.',// The :attribute may only contain letters and numbers.
    'array'                => 'Поле ":attribute" должно быть массивом.',// The :attribute must be an array.
    'before'               => 'Дата :attribute должна быть перед :date.',// The :attribute must be a date before :date.
    'between'              => [
        'numeric' => 'Поле ":attribute" должно быть между :min и :max.',// The :attribute must be between :min and :max.
        'file'    => 'Файл ":attribute" должен быть размером от :min до :max килобайт.',// The :attribute must be between :min and :max kilobytes.
        'string'  => 'Поле ":attribute" должно содержать от :min до :max символов.',// The :attribute must be between :min and :max characters.
        'array'   => 'Поле ":attribute" должно содержать от :min до :max значений.',// The :attribute must have between :min and :max items.
    ],
    'boolean'              => 'Поле ":attribute" должно быть Верно или Неверно.',// The :attribute field must be true or false.
    'confirmed'            => 'Подтверждение ":attribute" не совпадает.',// The :attribute confirmation does not match.
    'date'                 => 'Дата ":attribute" неверная.',// The :attribute is not a valid date.
    'date_format'          => 'Поле ":attribute" не соответствует формату :format.',// The :attribute does not match the format :format.
    'different'            => 'Поля ":attribute" и ":other" должны быть разными.',// The :attribute and :other must be different.
    'digits'               => 'Поле ":attribute" должно содержать :digits цифр.',// The :attribute must be :digits digits.
    'digits_between'       => 'Поле ":attribute" должно содержать от :min до :max цифр.',// The :attribute must be between :min and :max digits.
    'dimensions'           => 'Изображение ":attribute" имеет недопустимые размеры.',// The :attribute has invalid image dimensions.
    'distinct'             => 'Поле ":attribute" содержит повторяющееся значение.',// The :attribute field has a duplicate value.
    'email'                => 'Поле ":attribute" должно быть действительным адресом электронной почты.',// The :attribute must be a valid email address.
    'exists'               => 'Выбранное поле ":attribute" неверное.',// The selected :attribute is invalid.
    'file'                 => '":attribute" должен быть файлом.',// The :attribute must be a file.
    'filled'               => 'Требуется поле ":attribute".',// The :attribute field is required.
    'image'                => '":attribute" должно быть изображением.',// The :attribute must be an image.
    'in'                   => 'Выбранное поле ":attribute" неверное.',// The selected :attribute is invalid.
    'in_array'             => '":attribute" не существует в ":other".',// The :attribute field does not exist in :other.
    'integer'              => '":attribute" должно быть целым числом.',// The :attribute must be an integer.
    'ip'                   => '":attribute" должен быть действительным IP адресом.',// The :attribute must be a valid IP address.
    'json'                 => '":attribute" должно быть действительной JSON строкой.',// The :attribute must be a valid JSON string.
    'max'                  => [
        'numeric' => '":attribute" не должен превышать :max.',// The :attribute may not be greater than :max.
        'file'    => '":attribute" не должен превышать :max килобайт.',// The :attribute may not be greater than :max kilobytes.
        'string'  => '":attribute" не должен превышать :max символов.',// The :attribute may not be greater than :max characters.
        'array'   => '":attribute" не должен содержать больше, чем :max значений.',// The :attribute may not have more than :max items.
    ],
    'mimes'                => 'Тип файла ":attribute" должен быть: :values.',// The :attribute must be a file of type: :values.
    'min'                  => [
        'numeric' => '":attribute" должен быть хотя бы :min.',// The :attribute must be at least :min.
        'file'    => '":attribute" должен иметь размер хотя бы :min килобайт.',// The :attribute must be at least :min kilobytes.
        'string'  => '":attribute" должен содержать хотя бы :min символов.',// The :attribute must be at least :min characters.
        'array'   => '":attribute" должен иметь хотя бы :min значений.',// The :attribute must have at least :min items.
    ],
    'not_in'               => 'Поле ":attribute" неверное.',// The selected :attribute is invalid.
    'numeric'              => 'Поле ":attribute" должно быть цифрой.',// The :attribute must be a number.
    'present'              => 'Отсутствует поле ":attribute".',// The :attribute field must be present.
    'regex'                => 'Формат поля ":attribute" неверный.',// The :attribute format is invalid.
    'required'             => 'Поле ":attribute" обязательно к заполнению.',// The :attribute field is required.
    'required_if'          => 'Требуется поле ":attribute", когда :other равен :value.',// The :attribute field is required when :other is :value.
    'required_unless'      => 'Требуется поле ":attribute", только если :other в :values.',// The :attribute field is required unless :other is in :values.
    'required_with'        => 'Требуется поле ":attribute", когда представлены :values.',// The :attribute field is required when :values is present.
    'required_with_all'    => 'Требуется поле ":attribute", когда представлены :values.',// The :attribute field is required when :values is present.
    'required_without'     => 'Требуется поле ":attribute", когда :values отсутствуют.',// The :attribute field is required when :values is not present.
    'required_without_all' => 'Требуется поле ":attribute", когда все :values отсутствуют.',// The :attribute field is required when none of :values are present.
    'same'                 => 'Поля ":attribute" и ":other" должны совпадать.',// The :attribute and :other must match.
    'size'                 => [
        'numeric' => 'Поле ":attribute" должно быть :size.',// The :attribute must be :size.
        'file'    => '":attribute" должен иметь размер :size килобайт.',// The :attribute must be :size kilobytes.
        'string'  => 'Поле ":attribute" должно состоять из :size символов.',// The :attribute must be :size characters.
        'array'   => 'Поле ":attribute" должно содержать :size символов.',// The :attribute must contain :size items.
    ],
    'string'               => 'Поле ":attribute" должно быть строкой.',// The :attribute must be a string.
    'timezone'             => 'Поле ":attribute" должно быть верной зоной.',// The :attribute must be a valid zone.
    'unique'               => 'Значение ":attribute" уже занято.',// The :attribute has already been taken.
    'url'                  => 'Неверный формат поля ":attribute".',// The :attribute format is invalid.
    'phone' => 'Неверный формат номера телефона',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'Имя',
        'email' => 'Электронная почта',
        'surname' => 'Фамилия',
        'patronymic' => 'Отчество',
        'addres' => 'Адрес регистрации',
        'login' => 'Телефон',
        'question' => 'Вопрос',
        'day' => 'День',
        'year' => 'Год',
        'month' => 'Месяц',
        'index' => 'Индекс',
        'city' => 'Населенный пункт',
        'street' => 'Улица',
        'building' => 'Дом',
        'appart' => 'Квартира',
        'code' => 'Проверочный код',
    ],
];
