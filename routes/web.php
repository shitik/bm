<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::post('verificate', 'PhoneVerificationController@index');
Route::post('feedback', 'BaseController@sensFeedback');
Route::post('winners', 'BaseController@getWinners');

Route::group(['prefix' => 'personal'], function () {
	Route::get('/', 'BaseController@personalPage')->name('personal');
	//Route::get('check/add', 'BaseController@indexCheck')->name('user.add.check');
	//Route::post('check/add', 'BaseController@indexCheck');
	Route::get('check/list', 'BaseController@userListCheck')->name('user.list.check');
});

Route::group(['middleware' => 'auth'], function () {
	
	Route::group(['middleware' => ['admin']], function () {
		Route::group(['prefix' => 'admin'], function () {
			Route::get('sendsms', 'BaseController@sendOldSMS')->name('sendsms');
			Route::post('sendsms', 'BaseController@sendOldSMS');
			Route::get('check', 'BaseController@nextCheck')->name('check');
			Route::get('check/{check}/view', 'BaseController@viewCheckFile')->name('admin.view.check');
			Route::get('check/list', 'BaseController@listCheck')->name('check.list');
			Route::get('check/export', 'BaseController@exportCheck')->name('check.export');
			Route::post('check/export', 'BaseController@exportCheck');
			Route::get('check/export/files', 'BaseController@getExports')->name('check.export.files');
			Route::post('check/export/files', 'BaseController@getExports');
			Route::post('check/update/{check}', 'BaseController@updateCheck')->name('check.update');
			Route::get('prizes', 'BaseController@listPrizes')->name('admin.prizes');
			Route::get('prizes/add', 'BaseController@addPrize')->name('admin.prizes.add');
			Route::post('prizes/add', 'BaseController@addPrize');
			Route::get('prizes/{prize}/delete', 'BaseController@deletePrize')->name('admin.prizes.delete');
			Route::get('raffles', 'BaseController@listRaffles')->name('admin.raffles');
			Route::get('raffles/add', 'BaseController@addRaffle')->name('admin.raffles.add');
			Route::post('raffles/add', 'BaseController@addRaffle');
			Route::get('raffles/{raffle}/delete', 'BaseController@deleteRaffle')->name('admin.raffle.delete');
			Route::get('raffles/{raffle}/activate', 'BaseController@activateRaffle')->name('admin.raffle.activate');
			Route::get('raffles/{raffle}/winners', 'BaseController@listRaffleWinners')->name('admin.raffle.winners');
			Route::get('raffles/{raffle}/winners/add', 'BaseController@addRaffleWinners')->name('admin.raffle.winners.add');
			Route::post('raffles/{raffle}/winners/add', 'BaseController@addRaffleWinners');
			Route::get('raffles/{raffle}/winners/{winner}/delete', 'BaseController@deleteWinner')->name('admin.raffle.winners.delete');
		});
	});
});
Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

//Route::get('/home', 'BaseController@index')->name('home');
